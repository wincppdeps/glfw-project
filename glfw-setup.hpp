#ifndef _GLFW_SETUP_HPP_
#define _GLFW_SETUP_HPP_

#include <string>
#include <vector>
#include <GLFW/glfw3.h>

class GlfwProgram
{
public:
    GlfwProgram();
    virtual ~GlfwProgram();

    int Run(int argc, char* argv[]);

protected:
    virtual bool SetUp() = 0;
    virtual void Render() = 0;
    virtual void CleanUp() = 0;

    virtual void OnKeyAction(int key, int scancode, int action, int mods) = 0;
    virtual void OnResize(int width, int height) = 0;

    int width, height;
    std::string title;
    std::vector<std::string> args;

private:
    GLFWwindow* window;
    static GlfwProgram* program;

    static void KeyActionCallback(GLFWwindow*, int key, int scancode, int action, int mods)
    {
        if(GlfwProgram::program) GlfwProgram::program->OnKeyAction(key, scancode, action, mods);
    }

    static void ResizeCallback(GLFWwindow*, int width, int height)
    {
        if(GlfwProgram::program) GlfwProgram::program->OnResize(width, height);
    }

};

#endif // _GLFW_SETUP_HPP_
