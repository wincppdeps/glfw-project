#include <iostream>
#include "glfw-setup.hpp"

class Program : public GlfwProgram
{
    virtual bool SetUp();
    virtual void Render();
    virtual void CleanUp();

    virtual void OnKeyAction(int /*key*/, int /*scancode*/, int /*action*/, int /*mods*/) { }
    virtual void OnResize(int /*width*/, int /*height*/) { }

};

bool Program::SetUp()
{
    return true;
}

void Program::Render()
{

}

void Program::CleanUp()
{

}

int main(int argc, char* argv[])
{
    return Program().Run(argc, argv);
}
