cmake_minimum_required(VERSION 2.8)

get_filename_component(PROJECT_NAME ${CMAKE_SOURCE_DIR} NAME)
project(${PROJECT_NAME})

set(WIN_CPP_PREFIX_PATH $ENV{WIN_CPP_PREFIX_PATH}) 
if (NOT WIN_CPP_PREFIX_PATH) 
    message("Cannot find WIN_CPP_PREFIX_PATH") 
endif() 
if (NOT EXISTS "${WIN_CPP_PREFIX_PATH}") 
    message("${WIN_CPP_PREFIX_PATH} does not exist") 
else()
    set(CMAKE_PREFIX_PATH "${WIN_CPP_PREFIX_PATH}")
    set(CMAKE_MODULE_PATH "${WIN_CPP_PREFIX_PATH}/cmake-modules")
    set(ZLIB_ROOT ${WIN_CPP_PREFIX_PATH})
endif()

if(CMAKE_COMPILER_IS_GNUCXX)
   SET(ENABLE_CXX11 "-std=c++11")

   EXECUTE_PROCESS(COMMAND "${CMAKE_CXX_COMPILER} -dumpversion" OUTPUT_VARIABLE GCC_VERSION)
   if (GCC_VERSION VERSION_LESS 4.7)
	  SET(ENABLE_CXX11 "-std=c++0x")
   endif()

   SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ENABLE_CXX11}")
endif()

find_package(OPENGL REQUIRED)

find_package(GLM REQUIRED)
include_directories(${GLM_INCLUDE_DIR})

find_package(GLFW3 REQUIRED)
include_directories(${GLFW3_INCLUDE_DIR})

set(SRC_APP
    program.cpp
    glfw-setup.cpp
    glfw-setup.hpp
    )

add_executable(${PROJECT_NAME} ${SRC_APP})

target_link_libraries(${PROJECT_NAME}
    ${GLFW3_LIBRARY}
    ${OPENGL_LIBRARIES}
)
